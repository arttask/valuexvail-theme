<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>

<section class="about-section">
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="left-side">
				<div class="big-avatar" style="background-image: url('<?php the_field('avatar'); ?>');"></div>
        <?php the_field('left'); ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="right-side">
				<div class="top-text">
          <?php the_field('top_text'); ?>
				</div>
				<div class="list-block">
        <?php the_field('list'); ?>
				</div>
				<div class="bottom-block">
          <?php the_field('bottom'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<?php echo getSlideSection(); ?>
<?php
get_footer();
