(function () {
  let filteredPresentations = document.querySelectorAll(".filtered-presentation");
  if(filteredPresentations) {
    let lists = document.querySelectorAll(".filter ul li a");
    let showAll = function () {
      let presentations = document.querySelectorAll(".presentation");
      presentations.forEach(presentation => {
        presentation.classList.remove('hide');
      });

    };
    let hideAll = function () {
      let presentations = document.querySelectorAll(".presentation");
      presentations.forEach(presentation => {
        presentation.classList.add('hide');
      });
    }
    let showYear = function (year) {
      hideAll();
      let presentations = document.querySelectorAll(".presentation-"+year);
      presentations.forEach(presentation => {
        presentation.classList.remove('hide');
      });
    };
    lists.forEach(el => {
      el.addEventListener("click", ($evt) => {
        year = $evt.target.hash.substring(1);
        if(year == 'all') {
          showAll();
        } else {
          showYear(year);
        }
      }, false);
      
    });
  }
})();
