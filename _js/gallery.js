(function () {
  let isPhotos = document.querySelector(".photos");
  if(isPhotos) {
    let button = document.querySelector(".load-more");
    button.addEventListener("click", ($evt) => {
      $evt.preventDefault();
      photos = document.querySelectorAll(".photo-open");
      var lastItem = photos[photos.length-1];
      var data = {
        action: 'morephotos',
        lastUrl: lastItem.href
      };
      $.post( Config.url, data, function(response) {
        let wrapper = document.querySelector('.photos-row');
        wrapper.innerHTML += response;
        $("a.photo-open").fancybox({
          'transitionIn': 'elastic',
          'transitionOut': 'elastic'
        });
      });
      return false;
    });
  }
})();
