function initMap() {
    var element = document.getElementById('map');
    if(element) {
        var options = {
            zoom: 17,
            center: { lat: 39.614431, lng: -104.887666 }
        };
    
        var myMap = new google.maps.Map(element, options);
        var marker = new google.maps.Marker({
            position: { lat: 39.614431, lng: -104.887666 },
            icon: Config.theme_dir+'/_imgs/marker.png',
            map: myMap
        });
    }
};