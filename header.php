
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="/favicon.ico">
  <title>
  </title>
  <meta property="og:title" content="" />
  <meta property="og:description" content="" />
  <meta property="og:url" content="" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="" />
  <meta property="og:site_name" content="" />
  
	<?php wp_head(); ?>
</head>
<body>
  <header class="main-header">
    <div class="container-fluid">
      <?php get_template_part( 'parts/part', 'logo' ); ?>
      <?php get_template_part( 'parts/sec', 'topmenu' ); ?>
    </div>
    <div class="menu-button">
      <span></span>
    </div>
    