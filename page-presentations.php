<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>
<?php
  $args = [
    'category_name' => get_theme_mod('valuexvail_presentations_category'),
    'posts_per_page' => -1
  ];
  $the_query = new WP_Query( $args );

  $years = [];
  $presentationsContent = '';
  while ( $the_query->have_posts() ) {
    $the_query->the_post();
    $conference = get_field('conference');
    $date = get_field('conference_date', $conference->ID);
    $tmpDate = explode(" ", $date);
    $years[$tmpDate[1]] = $tmpDate[1];

    $thumbnail = get_the_post_thumbnail_url();
    if($thumbnail == NULL) {
      $thumbnail = get_the_post_thumbnail_url($conference->ID);
    }
    $presentationsContent .= '<div class="col-md-4 col-sm-6 presentation presentation-'.$tmpDate[1].'">'."\n";
    $presentationsContent .= '<a href="'.get_field('link').'"> <div class="post-img" style="background-image: url(\''
          .$thumbnail
          .'\')"></div></a>'."\n";
    $presentationsContent .= '</div>'."\n";
  }
  wp_reset_postdata();
?>

<section class="presentation-section filtered-presentation">
	<div class="filter">
		<ul>
			<li><a href="#all">all</a></li>
      <?php 
        foreach($years as $key => $year) {
          echo '<li><a href="#'.$year.'">'.$year.'</a></li>';
        }
      ?>
		</ul>
	</div>
		<div class="presentation-inner">
		<div class="container">
			<div class="row">
        <?php
          echo $presentationsContent;
        ?>
			</div>
		</div>
	</div>
</section>
<section class="bottom-block" style="background-image: url('<?php echo get_template_directory_uri()?>/_imgs/slider-section-bg.jpg');">
	<?php 
		dynamic_sidebar( 'signup-form-widget' ); 
		echo getCopyrightHTML(TRUE);
	?>
</section>

<?php
get_footer();
