<nav class="main-nav">
  <ul>
  <?php
    $menu_items = wp_get_nav_menu_items('main-menu');
    $menu_list = '';
    $page_id = get_queried_object_id();
    foreach( $menu_items as $menu_item ) {
      $link = $menu_item->url;
      $title = $menu_item->title;
      $menu_list .= '<li class="item">' ."\n";
      $menu_list .= '<a href="'.$link.'"';
      if(strtolower($title)=='apply') {
        $menu_list.= ' class="btn round-btn" ';
      }
      $menu_list.='>'.$title.'</a>' ."\n";
      $menu_list .= '</li>' ."\n";

    }
    echo $menu_list;
  ?>
  </ul>
</nav>