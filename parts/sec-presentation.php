<section class="presentation">

<div class="presentation-inner">
	<h2>Presentation from previous Conferences</h2>
	<div class="container">
		<div class="row">
      <?php
        $args = [
          'category_name' => get_theme_mod('valuexvail_presentations_category'),
          'posts_per_page' => 6
        ];
        $the_query = new WP_Query( $args );

        while ( $the_query->have_posts() ) {
          $the_query->the_post();
          $thumbnail = get_the_post_thumbnail_url();
          if($thumbnail == NULL) {
            $conference = get_field('conference');
            $thumbnail = get_the_post_thumbnail_url($conference->ID);
          }
          echo '<div class="col-md-4 col-sm-6">';
          echo '<a href="'.get_field('link').'"> <div class="post-img" style="background-image: url(\''
                .$thumbnail
                .'\')"></div></a>';
          echo '</div>';
        }
        wp_reset_postdata();
      ?>
		</div>
		<a href="/presentations" class="btn big-round-btn">Check out all presentations</a>
	</div>
</div>
</section>