<?php

function valuexvail_scripts() {
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700');
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/_css/bootstrap.min.css' );
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/_css/fancybox.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/_css/responsive.css' );
	wp_enqueue_style( 'theme-styles', get_template_directory_uri() . '/_css/styles.css' );


	wp_enqueue_script( 'script-jquery', get_template_directory_uri() . '/_js/jquery-3.2.0.min.js', [], false, true );
	wp_enqueue_script( 'script-bootstrap', get_template_directory_uri() . '/_js/bootstrap.min.js', [], false, true );
	wp_enqueue_script( 'script-slick', get_template_directory_uri() . '/_js/slick.min.js', [], false, true);
	wp_enqueue_script( 'script-font-awesome', 'https://use.fontawesome.com/c28784b6b4.js', [], false, true );
	wp_enqueue_script( 'script-map', get_template_directory_uri() . '/_js/map.js', [], false, true );
	wp_localize_script('script-map', 'Config', ['theme_dir' => get_template_directory_uri()]);
	wp_enqueue_script( 'script-googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCDFvV0pZu4Q2ugFZcoORXk_T7a7To-TXw&callback=initMap', [], false, true );
	wp_enqueue_script( 'script-template', get_template_directory_uri() . '/_js/template.js', [], false, true );
	wp_enqueue_script( 'script-fancybox', get_template_directory_uri() . '/_js/jquery.fancybox.js', [], false, true );
	wp_enqueue_script( 'presentations-filter', get_template_directory_uri() . '/_js/presentationsFilter.js', [], false, true );
	wp_enqueue_script( 'gallery', get_template_directory_uri() . '/_js/gallery.js', [], false, true );
	wp_localize_script('gallery', 'Config', ['url' => admin_url('admin-ajax.php')]);
}

add_action( 'wp_enqueue_scripts', 'valuexvail_scripts' );