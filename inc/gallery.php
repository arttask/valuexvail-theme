<?php

function getAllConferencesPosts() {
	$args = ['posts_per_page'   => -1,'category_name' => 'Conferences'];
  return get_posts( $args );
}

function getAllGalleries() {
	$gallery = [];
	$posts = getAllConferencesPosts();
	foreach($posts as $post) {
		$postGallery = acf_photo_gallery('gallery', $post->ID);
		$gallery = array_merge($gallery, $postGallery);
	}
	return $gallery;
}