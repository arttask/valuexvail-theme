<?php

function valuexvail_widgets_init() {
 
	register_sidebar( [
			'name'          => 'Sign Up Form Widget',
			'id'            => 'signup-form-widget',
			'before_widget' => '<div class="form-wrapper"><div class="form-title">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
	]);

	register_sidebar( [
		'name'          => 'Ask a Question Widget',
		'id'            => 'question-form-widget',
		'before_widget' => '<div class="form-container">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	]);

	register_sidebar( [
		'name'          => 'Apply Form Widget',
		'id'            => 'apply-form-widget',
		'before_widget' => '<div class="apply-form">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	]);

}

add_action( 'widgets_init', 'valuexvail_widgets_init' );