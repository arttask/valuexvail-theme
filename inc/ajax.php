<?php

function ajax_morephotos() {
	$lastImgUrl = $_POST['lastUrl'];
  $Gallery = getAllGalleries();
  
  $index = -1;
  foreach($Gallery as $img) {
    if($img['full_image_url'] === $lastImgUrl) {
      $index = 0;
    }
    if($index>-1) {
      if($index>0) {
        echo getPictureHTML($img);
      } 
      if($index>=3) {
        break;
      }
      $index++;
    }
  }

	wp_die();
}
add_action('wp_ajax_nopriv_morephotos', 'ajax_morephotos');
add_action('wp_ajax_morephotos', 'ajax_morephotos');