<?php


add_theme_support( 'custom-logo' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );


function get_categories_select() {
	$teh_cats = get_categories();
		 $results;
		 $count = count($teh_cats);
		 for ($i=0; $i < $count; $i++) {
			 if (isset($teh_cats[$i]))
				 $results[$teh_cats[$i]->slug] = $teh_cats[$i]->name;
			 else
				 $count++;
		 }
	 return $results;
 }

function valuexvail_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
  $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	
	$wp_customize->add_section('valuexvail_theme_options', [
    'title'    => __('Theme Options', 'valuexvail'),
    'priority' => 120,
  ]);

	$wp_customize->add_setting('valuexvail_presentations_category', [
    'default'        => 'uncategorized',
    'capability'     => 'edit_theme_options',
  ]);
  $wp_customize->add_control( 'valuexvail_presentations_category', [
    'settings' => 'valuexvail_presentations_category',
    'label'   => 'Presentations category',
    'section' => 'valuexvail_theme_options',
    'type'    => 'select',
    'choices' => get_categories_select()
	]);
	
	$wp_customize->add_setting('valuexvail_hotels_category', [
    'default'        => 'uncategorized',
    'capability'     => 'edit_theme_options',
  ]);
  $wp_customize->add_control( 'valuexvail_hotels_category', [
    'settings' => 'valuexvail_hotels_category',
    'label'   => 'Hotels category',
    'section' => 'valuexvail_theme_options',
    'type'    => 'select',
    'choices' => get_categories_select()
	]);
	
	$wp_customize->add_setting('valuexvail_slide_category', [
    'default'        => 'uncategorized',
    'capability'     => 'edit_theme_options',
  ]);
  $wp_customize->add_control( 'valuexvail_slide_category', [
    'settings' => 'valuexvail_slide_category',
    'label'   => 'Slide Photo Count',
    'section' => 'valuexvail_theme_options',
    'type'    => 'number'
  ]);


}


add_action( 'customize_register', 'valuexvail_customize_register' );