<?php

function valuexvail_register_menus() {
  register_nav_menus(
    [  
			'main-menu' => __( 'Main Menu' ),
		]
  );
}

add_action( 'init', 'valuexvail_register_menus' );