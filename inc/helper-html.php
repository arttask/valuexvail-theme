<?php

function getPictureHTML($img) {
	$html = '';
	$html .= '<div class="col-md-4 col-sm-6">'."\n";
	$html .= '<a href="'.$img['full_image_url'].'" class="photo-open">'."\n";
	$html .= '<img src="'.$img['full_image_url'].'" alt="">'."\n";
	$html .= '<img src="'.get_template_directory_uri().'/_imgs/eye-overlay.png" alt="overlay" class="eye">'."\n";
	$html .= '</a>'."\n";
	$html .= '</div>'."\n";

	return $html;
}

function getCopyrightHTML($withbg=FALSE) {
	$bgclass = $withbg? 'with-bg' : '';
	return '<div class="copyright '.$bgclass.'">valuexvail &#169; 2017</div>';
}

function getSlideBlock() {
	$imageCount = get_theme_mod('valuexvail_slide_category');
	$gallery = getAllGalleries();
	shuffle($gallery);
	$html = '';
	$html .= '<div class="slider-block">'."\n";
	$html .= 	'<h2>Some Photos from Previous Conferences</h2>'."\n";
	$html .= 	'<div class="slider-wrapper">'."\n";
	$html .= 		'<div class="photo-slider clearfix">'."\n";
	for($i=0; $i<=$imageCount; $i++) {
		$img = $gallery[$i];
		$html .= '<div class="slick-slide" style="background-image: url(\''.$img["full_image_url"].'\');"></div>'."\n";
	}
	$html .= 		'</div>'."\n";
	$html .= 	'</div>'."\n";
	$html .= 	getCopyrightHTML();
	$html .= '</div>'."\n";
	return $html;
}


function getSlideSection() {
	$html = '';
	$html .= '<section class="slider-section clearfix" style="background-image: url(\''.get_template_directory_uri().'/_imgs/slider-section-bg.jpg\');">'."\n";
	$html .= getSlideBlock();
	$html .= '</section>'."\n";

	return $html;
}