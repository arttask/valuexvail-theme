<?php
  get_header();
?>
</header>
<section class="main-section" style="background-image: url('<?php echo get_template_directory_uri()?>/_imgs/main-section-bg.jpg');">
	<div class="clip-overlay" style="height: 152.4px;">
		<svg class="svg-wave" width="" height="" version="1.1" viewBox="0 0 100 10" preserveAspectRatio="none" xmlns="//www.w3.org/2000/svg">
		<path class="front-wave" d="M0,0 C30,6 80,4 100,0 L100,10 L0,10 Z" fill="#4f8abb"></path>
	</svg>
</div>
<div class="title-container">
	<h1><?php the_field('next_conference_title'); ?></h1>
	<?php
		$nextConference = get_field('conference');
	?>
	<div class="location">
		<span class="date"><img src="<?php echo get_template_directory_uri()?>/_imgs/date-icon.png" alt="">
			<?php the_field('conference_date', $nextConference->ID); ?>
		</span>
		<span class="city"><img src="<?php echo get_template_directory_uri()?>/_imgs/city-icon.png" alt="">
			<?php the_field('conference_location', $nextConference->ID); ?>
		</span>
	</div>
</div>
<?php dynamic_sidebar( 'signup-form-widget' ); ?>
</section>

<?php get_template_part( 'parts/sec', 'presentation' ); ?>

<section class="slider-section clearfix" style="background-image: url('<?php echo get_template_directory_uri()?>/_imgs/slider-section-bg.jpg');">
	<div class="text-block clearfix">
		<?php
			$aboutPage = get_page_by_path( 'about-us' );
			$aboutId = $aboutPage->ID;
		?>
		<img src="<?php the_field('smal_avatar', $aboutId); ?>" alt="avatar">
		<div class="description">
			<h3><?php the_field('sub_title', $aboutId); ?></h3>
			<?php
				$text = get_field('top_text', $aboutId);
				echo substr($text,0,468); 
				echo '</p>';
			?>
			<a href="<?php echo get_permalink($aboutId); ?>" class="btn white-btn">learn more</a>
		</div>
	</div>
	<?php echo getSlideBlock(); ?>
</section>
<?php
  get_footer();
?>