<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>
<?php
  $gallery = getAllGalleries();
  $gallery = array_slice($gallery, 0, get_field('photos_count'));
?>
<section class="photos">
	<div class="container">
		<div class="row photos-row">
    <?php
      foreach($gallery as $img) {
        echo getPictureHTML($img);
      }
    ?>
		</div>
	</div>
	<a href="#" class="btn load-more">load more</a>
</section>
<section class="bottom-block" style="background-image: url('<?php echo get_template_directory_uri()?>/_imgs/slider-section-bg.jpg');">
	<?php 
		dynamic_sidebar( 'signup-form-widget' ); 
		echo getCopyrightHTML(TRUE);
	?>
</section>
<?php
get_footer();
