<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>
<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="section-margine">
            <?php
            while ( have_posts() ) {
              the_post();
              get_template_part( 'parts/content', 'page' );
            }?>
    </section>
  </main>
  <div class="copyright with-bg">valuexvail &#169; 2017</div>
</div>
<?php
get_footer();
