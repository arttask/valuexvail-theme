<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>
<section class="hotels">
	<div class="container">
		<div class="hotels-title">
			<h2><?php the_field('sub_title'); ?></h2>
			<p><?php the_field('description'); ?></p>
		</div>
		<div class="column-wrapper">
			<div class="row">
				<?php
					$args = [
						'category_name' => get_theme_mod('valuexvail_hotels_category'),
						'posts_per_page' => -1
					];
					$the_query = new WP_Query( $args );

					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						echo '<div class="col-md-6">'."\n";
						echo '<div class="column">'."\n";
						echo '<h3>';
						the_field('sub_title');
						echo '</h3>'."\n";
						the_field('description');
						echo "\n";
						echo '<p><span>Reservations:</span> ';
						the_field('phone');
						echo "\n";
						echo '<strong>to received discounted rate mention ';
						the_field('discount');
						echo '</strong></p>'."\n";
						echo '<p><span>Rates:</span> ';
						the_field('rates',false, false);
						echo '</p>'."\n";
						
						echo get_the_post_thumbnail();
						echo "</div>\n</div>";
						
					}
					wp_reset_postdata();
				?>
</div>
</div>
</div>
</section>
<section class="bottom-block" style="background-image: url('<?php echo get_template_directory_uri()?>/_imgs/slider-section-bg.jpg');">
	<?php 
		dynamic_sidebar( 'signup-form-widget' ); 
		echo getCopyrightHTML(TRUE);
	?>
</section>
<?php
get_footer();
