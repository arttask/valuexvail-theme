<?php

include_once 'inc/ajax.php';
include_once 'inc/scripts.php';
include_once 'inc/theme-settings.php';
include_once 'inc/menus.php';
include_once 'inc/widgets.php';
include_once 'inc/gallery.php';
include_once 'inc/helper-html.php';


/*Contact form 7 remove span*/
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

	$content = str_replace('<br />', '', $content);
			
	return $content;
});