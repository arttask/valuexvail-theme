<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>

<section class="apply-section">
		<div class="custom-container">
		<div class="text-wrapper">
		<?php the_field('apply_text') ?>
		</div>
		<?php dynamic_sidebar( 'apply-form-widget' ); ?>
		</div>
	</section>
<?php echo getSlideSection(); ?>
<?php
get_footer();
