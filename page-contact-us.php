<?php
get_header();
?>
<?php get_template_part( 'parts/part', 'title' ); ?>
</header>
<section class="contact-us">
	<div id="map"></div>
	<div class="form-wrapper clearfix">
		<div class="col-md-6">
			<div class="row">
				<div class="form-container">
					<?php dynamic_sidebar( 'question-form-widget' ); ?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="address-container">
				<h3>Contact Information</h3>
				<p>Address: <strong><?php the_field('address'); ?></strong></p>
				<p>Email: <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
			</div>
		</div>
	</div>
</section>
<?php echo getSlideSection(); ?>
<?php
get_footer();
